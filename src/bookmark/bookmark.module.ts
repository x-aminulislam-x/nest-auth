import { Module } from '@nestjs/common';
import { BookmarkController } from './bookmark.controller';
import { ControllerService } from './controller/controller.service';
import { BookmarkService } from './bookmark.service';

@Module({
  controllers: [BookmarkController],
  providers: [ControllerService, BookmarkService],
})
export class BookmarkModule {}
